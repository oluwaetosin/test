class DivisionHandler{
   
    /**
     * @param {number} numerator
     * @param {number} denominator
     * @constructor
     */
   constructor(numerator, denominator) {
    this.numerator = numerator; 
    this.denominator = denominator;
    this.quotient = null;
    this.remainder = null;
   }
    /**
     *  
     * @description returns a range of possible quotient values
     *  from the numerator and denominator of the class instance
     * @returns {Array} array of numbers
     */
   getQuotientRange() {
        let maxNum = this.numerator - this.denominator >= 0 ? this.numerator : this.denominator;

        let minimumNum = maxNum === this.numerator ? this.denominator : this.numerator;

        return this.getNumberRange(maxNum, minimumNum);
   }
   /**
    *@description get validQuotient from a range of possibele quotient
    * @returns {object} e.g {quotient: 4, remainder: -1}
    */
   getValidQuotient() {

   }

   getNumberRange(maxNumber, minNumber){
       let range = [];
       while(minNumber <= maxNumber){
           range.push(minNumber);
           minNumber++;
       }
       return range;
   }

   binarySearch(numArray){
        const middleIndex = Math.floor(numArray.length/2);
        const middleNum = numArray[middleIndex];

        if(numArray.length > 1){
            if(typeof(numArray[middleIndex + 1]) != 'undefined' && this.itsAMatch(numArray[middleIndex + 1])){
                return this.binarySearch(numArray.splice(middleIndex,numArray.length));
            }
            else if(typeof(numArray[middleIndex - 1]) != 'undefined'){
                return this.binarySearch(numArray.splice(0,middleIndex));
            }
            else if(this.itsAMatch(numArray[middleIndex])){
               return {quotient:numArray[middleIndex], remainder : (this.numerator - (this.denominator * numArray[middleIndex]))};
            }
            else if(this.itsAMatch(1)){
                return {quotient:1, remainder : (this.numerator - (this.denominator * 1))};
             }
        }
        else{
            if(this.itsAMatch(middleNum)){
                return {quotient:middleNum, remainder : (this.numerator - (this.denominator * middleNum))};
            }
            else{
                return {quotient:1, remainder : (this.numerator - (this.denominator * 1))};
            }
           
        }


   }

   itsAMatch(quotient){
       const remainder =  this.numerator - (this.denominator * quotient);

       const absRemainder = remainder < 0 ? remainder * -1 : remainder;

       
       const absDenominator = this.denominator < 0 ? this.denominator * -1  : this.denominator;

       return absDenominator > absRemainder;
   }




}
 export default DivisionHandler;