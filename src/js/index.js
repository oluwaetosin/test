require('../../node_modules/bootstrap/dist/css/bootstrap.min.css');
require('../css/main.css');
import DivisionHandler from './divisionhandler';
import TwitterSearch from './tweetmanager';
window.onload = ()=>{
  
    let numerator = document.getElementById('numerator').value;
    let denominator = document.getElementById('denominator').value;
    console.log(numerator,denominator);
    document.getElementById("getQuotients").addEventListener('click',((e)=>{
        let numerator = parseInt(document.getElementById('numerator').value);
        let denominator = parseInt(document.getElementById('denominator').value);
         
        console.log(numerator,denominator);
        let handler =  new DivisionHandler(numerator,denominator);
        const quotientAndRemainder = handler.binarySearch(handler.getQuotientRange());
        console.log(quotientAndRemainder);
        document.getElementById("numeratorResult").textContent = numerator;
        document.getElementById("denominatorResult").textContent = denominator;
        document.getElementById("quotientResult").textContent = quotientAndRemainder['quotient'];
        document.getElementById("remainderResult").textContent = quotientAndRemainder['remainder'];

        e.preventDefault();
    }));



    document.getElementById("feed").addEventListener('click',((e)=>{
        const feed = document.getElementById("feed").value;
        let twitterManager =  new TwitterSearch();
        twitterManager.getLatestTweet(feed);
        e.preventDefault();
    }));
}
