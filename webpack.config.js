var path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
  entry: './src/js/index.js',
  output:{
    path: __dirname + '/dist',
    filename: 'main.js'
    
  },
  
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 8000
  },
  module:{
      rules:[
        {test: /\.css$/, loader: 'style-loader!css-loader'},
        {test: /\.js$/, loader: 'babel-loader', exclude: '/node_modules/', query:{presets:['es2015']}}
      ]
  },
  plugins:[  new HtmlWebpackPlugin({
    template: 'index.html',
    extraFiles: {
      css: __dirname + 'node_modules/bootstrap/dist/css/bootstrap.min.css'
    }
  })]
};